package com.whereta.mapper;

import com.whereta.model.DepartmentAccount;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

@Component("departmentAccountMapper")
public interface DepartmentAccountMapper {
    /**
     * 根据主键删除
     * 参数:主键
     * 返回:删除个数
     * @ibatorgenerated 2015-09-02 09:49:12
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 插入，空属性也会插入
     * 参数:pojo对象
     * 返回:删除个数
     * @ibatorgenerated 2015-09-02 09:49:12
     */
    int insert(DepartmentAccount record);

    /**
     * 插入，空属性不会插入
     * 参数:pojo对象
     * 返回:删除个数
     * @ibatorgenerated 2015-09-02 09:49:12
     */
    int insertSelective(DepartmentAccount record);

    /**
     * 根据主键查询
     * 参数:查询条件,主键值
     * 返回:对象
     * @ibatorgenerated 2015-09-02 09:49:12
     */
    DepartmentAccount selectByPrimaryKey(Integer id);

    /**
     * 根据主键修改，空值条件不会修改成null
     * 参数:1.要修改成的值
     * 返回:成功修改个数
     * @ibatorgenerated 2015-09-02 09:49:12
     */
    int updateByPrimaryKeySelective(DepartmentAccount record);

    /**
     * 根据主键修改，空值条件会修改成null
     * 参数:1.要修改成的值
     * 返回:成功修改个数
     * @ibatorgenerated 2015-09-02 09:49:12
     */
    int updateByPrimaryKey(DepartmentAccount record);

    Integer getDepIdByAccountId(@Param("accountId") int accountId);

    int delete(@Param("accountId") Integer accountId, @Param("depId") Integer depId);
}