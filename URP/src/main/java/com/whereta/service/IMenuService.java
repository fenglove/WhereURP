package com.whereta.service;

import com.whereta.vo.MenuCreateVO;
import com.whereta.vo.MenuEditVO;
import com.whereta.vo.ResultVO;

import java.util.List;
import java.util.Map;

/**
 * Created by vincent on 15-8-27.
 */
public interface IMenuService {

    List<Map<String,Object>> selectShowMenus(Integer rootId);

    ResultVO createMenu(MenuCreateVO createVO);

    ResultVO deleteMenu(int menuId);

    ResultVO editMenu(MenuEditVO editVO);

    ResultVO grantPermissions(int menuId, Integer[] perIdArray);
}
