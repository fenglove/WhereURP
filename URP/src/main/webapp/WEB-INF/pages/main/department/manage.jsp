<%--
  Created by IntelliJ IDEA.
  User: heli
  Date: 2015/8/28
  Time: 9:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title></title>
</head>
<body>
<table id="main-department-manage-department-tb" fit="true"></table>
<div id="main-department-manage-add-dialog" title="添加部门" style="width:400px;height:200px;display: none;"
     data-options="iconCls:'icon-add',resizable:false,modal:true">

  <form method="post" id="main-department-manage-add-dialog-form">
    <table border="0" align="center">
      <tr>
        <td>上级部门</td>
        <td>
          <select id="main-department-manage-add-dialog-parent-deplist" name="parentId"
                  style="width:200px;height: 30px;"
                  data-options="url:'${pageContext.request.contextPath}/main/department/get-show-departments.html',required:true,lines:true,missingMessage:'请选择上级部门'">
          </select>
        </td>
      </tr>
      <tr>
        <td>部门名字</td>
        <td>
          <input class="easyui-textbox" style="width:200px;height: 30px;" name="name" data-options="required:true,validType:'deparetmentName[1,30]',missingMessage:'请输入部门名字'">
        </td>
      </tr>
      <tr>
        <td>部门排序</td>
        <td>
          <input class="easyui-textbox" style="width:200px;height:30px;" name="order" data-options="required:true,validType:'order[1,5]',missingMessage:'请输入部门排序'">
        </td>
      </tr>
    </table>
  </form>
</div>
<div id="main-department-manage-edit-dialog" title="编辑部门" style="width:400px;height:230px;display: none;"
     data-options="iconCls:'icon-edit',resizable:false,modal:true">
  <form method="post" id="main-department-manage-edit-dialog-form">
    <table border="0" align="center">
      <tr>
        <td>上级部门</td>
        <td>
          <select id="main-department-manage-edit-dialog-parent-departmentlist" name="parentId"
                  style="width:200px;height: 30px;"
                  data-options="url:'${pageContext.request.contextPath}/main/department/get-show-departments-except-children.html',required:true,lines:true,missingMessage:'请选择父级权限'">
          </select>
        </td>
      </tr>
      <tr>
        <td>部门名字</td>
        <td>
          <input id="main-department-manage-edit-dialog-form-name" class="easyui-textbox" style="width:200px;height: 30px;" name="name" data-options="required:true,validType:'permissionName[1,30]',missingMessage:'请输入权限名字'">
        </td>
      </tr>
      <tr>
        <td>部门排序</td>
        <td>
          <input id="main-department-manage-edit-dialog-form-order"  class="easyui-textbox" style="width:200px;height:30px;" name="order" data-options="required:true,validType:'order[1,5]',missingMessage:'请输入权限排序'">
        </td>
      </tr>
    </table>
  </form>
</div>

<script>
  seajs.use(['base','main/department/manage'], function (b,m) {
    b.init();
    m.init('${pageContext.request.contextPath}');
  });
</script>
</body>
</html>
