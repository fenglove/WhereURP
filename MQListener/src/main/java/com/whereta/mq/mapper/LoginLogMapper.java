package com.whereta.mq.mapper;

import com.whereta.mq.model.LoginLog;
import org.springframework.stereotype.Component;

@Component("loginLogMapper")
public interface LoginLogMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(LoginLog record);

    int insertSelective(LoginLog record);

    LoginLog selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LoginLog record);

    int updateByPrimaryKey(LoginLog record);
}