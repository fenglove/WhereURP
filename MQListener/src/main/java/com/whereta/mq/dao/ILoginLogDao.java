package com.whereta.mq.dao;

import com.whereta.mq.model.LoginLog;

/**
 * Created by vincent on 15-9-10.
 */
public interface ILoginLogDao {

    int save(LoginLog loginLog);
}
