SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for account_role
-- ----------------------------
DROP TABLE IF EXISTS `blog_category`;
CREATE TABLE `blog_category`(
  id int PRIMARY KEY AUTO_INCREMENT,
  create_date DATETIME NOT NULL COMMENT '创建时间',
  content VARCHAR(200) NOT NULL COMMENT '类别内容'
)ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;